package services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.provider.Browser;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by hounge on 9/25/2014.
 */

public class BackgroundService extends Service {
    public static final String Stored_Token = "TokenFile";
    private final Handler handler = new Handler();


    public Runnable sendUpdatesToUI = new Runnable()
    {
        public void run()
        {

            BackgroundService.this.DisplayLoggingInfo();
            BackgroundService.this.handler.postDelayed(this, 15000);
            Date myDate = new Date();
            SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
            // Format the date to Strings
            String smdy = mdyFormat.format(myDate);
            // Results...
            SharedPreferences.Editor appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
            appPrefEditor.putString("StartTime", smdy);
            appPrefEditor.apply();
        }
    };

    private void DisplayLoggingInfo() {
        try {
            SharedPreferences appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
            SharedPreferences.Editor appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
            String Token = appPref.getString("Token", null);
            String smdy = appPref.getString("StartTime", null);
            String url = appPref.getString("Browser", null);
            String option = GetCurrentActivity();
            if(option == null){
                return;
            }

            Date myDate = new Date();

            SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
            // Format the date to Strings
            String emdy = mdyFormat.format(myDate);
            //Create JSONObject to register with server
            JSONObject register = new JSONObject();
            register.put("Token",Token);
            register.put("Date1", smdy);
            register.put("Date2", emdy);
            register.put("Option", option);
            register.put("Str", url);

            final String urla="http://www.incpon.com/inkpon/checkin";
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //Post JSONObject to server
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(urla);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpPost.setEntity(new StringEntity(register.toString(), HTTP.UTF_8));
            //gets Response from URL
            HttpResponse responseGet = client.execute(httpPost);
            String response1 = EntityUtils.toString(responseGet.getEntity());
            appPrefEditor.remove(url).commit();
            Log.i("I like cake", response1);
        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    private void GetChromeURL() {
        Cursor cur = getContentResolver().query(Browser.BOOKMARKS_URI,
                new String[]{Browser.BookmarkColumns.URL}, null, null,
                Browser.BookmarkColumns.DATE + " DESC");
        if (cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            String url = cur.getString(cur
                    .getColumnIndex(Browser.BookmarkColumns.URL));
            cur.close();
            Log.i("browser", url);
            SharedPreferences.Editor appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
            appPrefEditor.putString("Browser", url);
            appPrefEditor.apply();
            return;
        } else {
            if (cur != null) {
                cur.close();
            }
            return;
        }


    }


    /*this code should work for android browser and Chrome */
    private void GetBrowserURL() {
        Cursor cur = getContentResolver().query(Browser.BOOKMARKS_URI,
                new String[]{Browser.BookmarkColumns.URL}, null, null,
                Browser.BookmarkColumns.DATE + " DESC");
        if (cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            String url = cur.getString(cur
                    .getColumnIndex(Browser.BookmarkColumns.URL));
            cur.close();
            Log.i("browser", url);
            SharedPreferences.Editor appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
            appPrefEditor.putString("Browser", url);
            appPrefEditor.apply();
            return;
        } else {
            if (cur != null) {
                cur.close();
            }
            return;
        }


    }

    public void GetFirefoxUrl(){
        //needs converted from Js to Java/android
      /*  var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);
        var recentWindow = wm.getMostRecentWindow("navigator:browser");
        SharedPreferences.Editor appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        appPrefEditor.putString("Browser", recentWindow);
        appPrefEditor.apply(); */
        return ;

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.handler.removeCallbacks(this.sendUpdatesToUI);
        this.handler.postDelayed(this.sendUpdatesToUI, 1000);

        //Start the service thread.
        MyThread thethread = new MyThread(this);
        thethread.start();

        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }





    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){

        stopSelf();
        super.onDestroy();
        handler.removeCallbacks(sendUpdatesToUI);
        Intent homeIntent= new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }



    private class MyThread extends Thread{
        //run service in it's own thread.
        Context context;

        public MyThread(Context context){
             this.context = context;
        }

        public void run(){
            //the service code that will actually run.
        }

    }






    public String GetCurrentActivity(){

        //initialize Activity Manager
        ActivityManager activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> getRunningTaskInfo = activityManager.getRunningTasks(2);

        ComponentName componentInfo = getRunningTaskInfo.get(0).topActivity;
        String ClassName = getRunningTaskInfo.get(0).topActivity.getClassName();
        String PackageName = componentInfo.getPackageName();
        String Results = GetProperAppName(PackageName);

        //this does not log Launcher to server
        if(Results.toString().equals("Launcher")){
            return null;
        }
        //this does not log Main to server
        if(Results.toString().equals("Main")){
            return null;
        }
        //this does not log Inkpon to server
        if(Results.toString().equals("Inkpon")){
            return null;
        }
        if(Results.toString().equals("Chrome")){
            GetChromeURL();
        }
        if(Results.toString().equals("Browser")){
            GetBrowserURL();
        }
        if(Results.toString().equals("Firefox")){
            //Firefox changed the way they log url from one version to the next.
            GetFirefoxUrl();
        }
        return Results;
    }

    public String GetProperAppName(String PackageName){

        final PackageManager pm = getApplicationContext().getPackageManager();
        try {
            ApplicationInfo AppInfo = pm.getApplicationInfo(PackageName, 1);
            if (AppInfo != null) {
                String AppName = (String) pm.getApplicationLabel(AppInfo);
                return AppName;
            } else {

                String AppName = "Inkpon: Unknown app";
                return AppName;
            }
        }
            catch (PackageManager.NameNotFoundException e) {

                String Error = "Inkpon Service Error: " + e.toString();

                return Error;
            }
    }
}