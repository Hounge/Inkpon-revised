package services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by hounge on 10/3/14.
 */
public class ParseJSONService extends Service {
    public static final String Stored_Token = "TokenFile";



    public void onCreate() {
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        String GETUSERINFO = prefs.getString("GETUSERINFO", null);
        Log.i("TAG", "Started Parsing");


        try {
            JSONArray jArr = new JSONArray(GETUSERINFO);
            for (int i = 0; i < jArr.length(); i++) {

                JSONObject obj = jArr.getJSONObject(i);
                String ActiveFlag= obj.getString("ActiveFlag");
                String AgeGroup= obj.getString("AgeGroup");
                String CanViewGraph= obj.getString("CanViewGraph");
                String CellPhone= obj.getString("CellPhone");
                String City= obj.getString("City");
                String CreatedDate= obj.getString("CreatedDate");
                String CurrentBalance= obj.getString("CurrentBalance");
                String DateOfBirth= obj.getString("DateOfBirth");
                String Email= obj.getString("Email");
                String Ethnicity= obj.getString("Ethnicity");
                String FirstName= obj.getString("FirstName");
                String Gender= obj.getString("Gender");
                String HomePhone= obj.getString("HomePhone");
                String I_ArtsCrafts= obj.getString("I_ArtsCrafts");
                String I_ComputersIT= obj.getString("I_ComputersIT");
                String I_Cycling= obj.getString("I_Cycling");
                String I_Dance= obj.getString("I_Dance");
                String I_Exercise= obj.getString("I_Exercise");
                String I_Gaming= obj.getString("I_Gaming");
                String I_HealthFitness= obj.getString("I_HealthFitness");
                String I_Shopping= obj.getString("I_Shopping");
                String I_Theatre= obj.getString("I_Theatre");
                String LastName= obj.getString("LastName");
                String MemberSince= obj.getString("MemberSince");
                String Password= obj.getString("Password");
                String State= obj.getString("State");
                String StreetAddress1= obj.getString("StreetAddress1");
                String StreetAddress2= obj.getString("StreetAddress2");
                String UserID= obj.getString("UserID");
                String UserName= obj.getString("UserName");
                String Zip= obj.getString("Zip");
                String picture= obj.getString("picture");

                editor.putString("ActiveFlag",ActiveFlag);
                editor.putString("AgeGroup",AgeGroup);
                editor.putString("CanViewGraph",CanViewGraph);
                editor.putString("CellPhone",CellPhone);
                editor.putString("City",City);
                editor.putString("CreatedDate",CreatedDate);
                editor.putString("CurrentBalance",CurrentBalance);
                editor.putString("DateOfBirth",DateOfBirth);
                editor.putString("Email",Email);
                editor.putString("Ethnicity",Ethnicity);
                editor.putString("FirstName",FirstName);
                editor.putString("Gender",Gender);
                editor.putString("HomePhone",HomePhone);
                editor.putString("I_ArtsCrafts",I_ArtsCrafts);
                editor.putString("I_ComputersIT",I_ComputersIT);
                editor.putString("I_Cycling",I_Cycling);
                editor.putString("I_Dance",I_Dance);
                editor.putString("I_Exercise",I_Exercise);
                editor.putString("I_Gaming",I_Gaming);
                editor.putString("I_HealthFitness",I_HealthFitness);
                editor.putString("I_Shopping",I_Shopping);
                editor.putString("I_Theatre",I_Theatre);
                editor.putString("LastName",LastName);
                editor.putString("MemberSince",MemberSince);
                editor.putString("Password",Password);
                editor.putString("State",State);
                editor.putString("StreetAddress1",StreetAddress1);
                editor.putString("StreetAddress2",StreetAddress2);
                editor.putString("UserID",UserID);
                editor.putString("UserName",UserName);
                editor.putString("Zip",Zip);
                editor.putString("picture",picture);
                editor.apply();
                //Log.i("TAG", FirstName);




            }
        }catch (JSONException e) {
            e.printStackTrace();
        }


        //Turns off service after complete
       stopSelf();
    }


            @Override
            public IBinder onBind (Intent intent){
                return null;
            }
        }
