package services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import com.inkpon.app.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import services.ParseJSONService;

/**
 * Created by hounge on 10/4/14.
 */
public class GetuserinfoService extends Service {
    public static final String Stored_Token = "TokenFile";


    public void onCreate() {
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        String Token = prefs.getString("Token", null);
        Resources resources=getResources();
        String get_user_info=String.format(resources.getString(R.string.get_user_info));
        //Log.i("APICALL", get_user_info); //testing to see values are correct

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //creates new JSON object to send with Post contains Token from login
            JSONObject parametersList = new JSONObject();
            parametersList.put("Token", Token);
            //System.out.println(Token);
            //sets headers and Posts to URL
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(get_user_info);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpPost.setEntity(new StringEntity(parametersList.toString(), HTTP.UTF_8));
            //gets Response from URL
            HttpResponse responseGet = client.execute(httpPost);
            //converts Response to String and store in shared preferences
            int statusCode = responseGet.getStatusLine().getStatusCode();
            String jsonResponse = EntityUtils.toString(responseGet.getEntity());
            editor.putString("GETUSERINFO", jsonResponse);
            editor.apply();
            //get status codes from URL and log response for debug
            if (statusCode != HttpStatus.SC_OK) {
               // Log.i("TAG", "Error in web request: " + statusCode);
               // Log.i("TAG", jsonResponse);
            } else {
               // Log.i("TAG", jsonResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }



        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
