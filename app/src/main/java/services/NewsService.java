package services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;


import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by hounge on 10/15/14.
 */
public class NewsService extends Service {

    public static final String Stored_Token = "TokenFile";
    ArrayList<String> NewsID = new ArrayList<String>();
    ArrayList<String> Message = new ArrayList<String>();


    public void onCreate() {
        SharedPreferences appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        String APICALL = "http://www.incpon.com/inkpon/News";
        Log.i("APICALL", APICALL); //testing to see values are correct


        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //sets headers and Posts to URL
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(APICALL);
            //gets Response from URL
            HttpResponse responseGet = client.execute(httpGet);
            //converts Response to String and store in shared preferences
            int statusCode = responseGet.getStatusLine().getStatusCode();
            String jsonResponse = EntityUtils.toString(responseGet.getEntity());
            SharedPreferences.Editor appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
            appPrefEditor.putString("JSONARRAY", jsonResponse);
            appPrefEditor.apply();
            //get status codes from URL and log response for debug
            if (statusCode != HttpStatus.SC_OK) {
                Log.i("TAG", "Error in web request: " + statusCode);
                Log.i("TAG", jsonResponse);
            } else {
                Log.i("TAG", jsonResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            String jsonResponse = appPref.getString("JSONARRAY", null);
            JSONArray jArr = new JSONArray(jsonResponse);
            for (int i = 0; i < jArr.length(); i++) {
                try {
                    JSONObject jsonObject = jArr.getJSONObject(i);
                    NewsID.add(jsonObject.getString("NewsID").toString());
                    Message.add(jsonObject.getString("Message").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void PopToast(String Message){

        Context context = getApplicationContext();

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, Message, duration);

        toast.show();

    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}

