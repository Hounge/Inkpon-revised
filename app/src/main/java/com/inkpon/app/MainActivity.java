package com.inkpon.app;

import android.app.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import services.BackgroundService;
import services.ChatService;
import services.NewsService;


public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    public static final String Stored_Token = "TokenFile";
    SharedPreferences appPref;
    SharedPreferences.Editor appPrefEditor;

    private NavigationDrawerFragment mNavigationDrawerFragment;


    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();

        setContentView(R.layout.activity_main);
        Intent i = new Intent(this, NewsService.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(i);


        Intent id = new Intent(this, BackgroundService.class);
        id.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(id);

        Intent ic = new Intent(this, ChatService.class);
        ic.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(ic);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }
    public void EditProfileClick(View view) {

        Intent i = new Intent(MainActivity.this, EditProfile.class);
        startActivity(i);
    }
    public void AccountDetailsClick(View view) {
        Resources resources=getResources();
        String RPT_user_total_minutes=String.format(resources.getString(R.string.RPT_user_total_minutes));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", RPT_user_total_minutes);
        editor.apply();
        Log.i("APICALL", RPT_user_total_minutes);
        Intent i = new Intent(MainActivity.this, AccountDetails.class);
        startActivity(i);

    }
    public void ChatClick(View view) {
        Resources resources=getResources();
        String get_user_chat_history =String.format(resources.getString(R.string.get_user_chat_history));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", get_user_chat_history );
        editor.apply();
        Log.i("APICALL", get_user_chat_history);
        Intent i = new Intent(MainActivity.this, Chat.class);
        startActivity(i);

    }
    public void TimetransferClick(View view) {
        Resources resources=getResources();
        String transfer_get_hours =String.format(resources.getString(R.string.transfer_get_hours));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", transfer_get_hours );
        editor.apply();
        Log.i("APICALL", transfer_get_hours);
        Intent i = new Intent(MainActivity.this, TransactionHistory.class);
        startActivity(i);

    }
    public void DealsClick(View view) {
        Resources resources=getResources();
        String query_coupons =String.format(resources.getString(R.string.query_coupons));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", query_coupons );
        editor.apply();
        Log.i("APICALL", query_coupons);
        Intent i = new Intent(MainActivity.this, Deals.class);
        startActivity(i);

    }
    public void SummaryClick(View view) {
        Resources resources=getResources();
        String get_user_withdrawal =String.format(resources.getString(R.string.get_user_withdrawal));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", get_user_withdrawal);
        editor.apply();
        Log.i("APICALL", get_user_withdrawal);
        Intent i = new Intent(MainActivity.this, Summary.class);
        startActivity(i);

    }

    //Toast message for InkPon News
    public void PopToast(String msg){

        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, msg, duration);

        toast.show();

    }

    /**
     * ---added by cenwesi ***
     */

    @Override
    protected void onDestroy() {
        logOut();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        logOut();
        super.onBackPressed();
    }


    private void logOut() {
        String Token = appPref.getString("Token", null);
        String url = getResources().getString(R.string.logout);

        if (Token != null) {
            try {
                JSONObject Details = new JSONObject();
                Details.put("Token", Token);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                httppost.setEntity(new StringEntity(Details.toString(), HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);

            /*--check http status code...*/
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode != HttpStatus.SC_OK) {
                    Log.d("***Logout: ", "Error login out...");
                } else {
                /* remove the token */
                    //SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
                    appPrefEditor.remove("Token");
                    appPrefEditor.apply();
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


        @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                //Intent profileIntent = new Intent(this,Activity.class);
                //startActivity(profileIntent);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                //Intent accountIntent = new Intent(this,Activity.class);
                //startActivity(accountIntent);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                //Intent chatIntent = new Intent(this,Activity.class);
                //startActivity(chatIntent);
                break;
            case 4:
                mTitle = getString(R.string.title_section4);
                //Intent transferIntent = new Intent(this,Activity.class);
                //startActivity(transferIntent);
                break;
            case 5:
                mTitle = getString(R.string.title_section5);
                //Intent dealsIntent = new Intent(this,Activity.class);
                //startActivity(dealsIntent);
                break;
            case 6:
                mTitle = getString(R.string.title_section6);
                //Intent summaryIntent = new Intent(this,Activity.class);
                //startActivity(summaryIntent);
                break;
            case 7:
                mTitle = getString(R.string.title_section7);
                //Intent summaryIntent = new Intent(this,Activity.class);
                //startActivity(summaryIntent);
                break;
            case 8:
                mTitle = getString(R.string.title_section8);
                //Intent logoutIntent = new Intent(this,Activity.class);
                //startActivity(logoutIntent);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {


            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
