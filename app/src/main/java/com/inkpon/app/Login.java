package com.inkpon.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import services.BackgroundService;
import services.GetuserinfoService;

/**
 * Created by hounge on 9/25/2014.
 */
public class Login extends Activity {
    public static final String Stored_Token = "TokenFile";
    //    public static final String InkPonApp = "InkPonApp";
    EditText UserName;
    EditText Pass;

    SharedPreferences appPref;
    SharedPreferences.Editor appPrefEditor;
    Boolean isLoggedIn = false;


    private ProgressDialog simpleWaitDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();

        setContentView(R.layout.activity_login);
        // Set up the login form.
        UserName = (EditText) findViewById(R.id.UserName);
        Pass = (EditText) findViewById(R.id.Pass);

        boolean rememberme = appPref.getBoolean("RememberMe",false);
        if (rememberme){
            CheckBox rmb = (CheckBox) findViewById(R.id.rememberMe);
            rmb.setChecked(rememberme);

            String InkPonUser = atob( appPref.getString("InkPonUser",null) );
            String[] parts = InkPonUser.split(":");

            UserName.setText( parts[0]);
            Pass.setText( parts[1]);
        }
    }





    public void ForgottenClick(View view) {
        Intent intent = new Intent(Login.this, ForgotPassword.class);
        startActivity(intent);
    }
    //   used during testing to clear stored preferences
    public void ClearAll(View view) {
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.clear().commit();
        Log.i("TAG", "ALL CLEAR");
    }

    public void UserRegister(View view) {
        Intent intent = new Intent(Login.this, Register.class);
        startActivity(intent);
    }

    public void loginClick(View view) {
        // Validate the log in data
        boolean validationError = false;
        StringBuilder validationErrorMessage =
                new StringBuilder(getResources().getString(R.string.error_intro));
        if (isEmpty(UserName)) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_username));
        }
        if (isEmpty(Pass)) {
            if (validationError) {
                validationErrorMessage.append(getResources().getString(R.string.error_join));
            }
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_password));
        }
        validationErrorMessage.append(getResources().getString(R.string.error_end));
        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(Login.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                    .show();
            return;
        }

        /*--resave option just incase */
        savepreference();

        final ProgressDialog dlg = new ProgressDialog(Login.this);
        dlg.setTitle("Please wait.");
        dlg.setMessage("Logging in.  Please wait.");
        //dlg.show();

        MessageDigest encoded = null;
        try {
            encoded = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String text = Pass.getText().toString();
        try {
            encoded.update(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.v("ewwor", "someting wong");
            e.printStackTrace();
        }
        byte[] passhash = encoded.digest();
        String hshed2 = passhash.toString();
        //Log.i("pasword", hshed2);
        BigInteger bigInt = new BigInteger(1, passhash);
        String hashed = bigInt.toString(16);
        //Log.i("pasword", hashed);
        String Username = UserName.getText().toString();
        String token = (Username + ":" + hashed);

        byte[] data = new byte[0];
        try {
            data = token.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String encodedhash = Base64.encodeToString(data, Base64.NO_WRAP);
        final String basicAuth = "Basic " + encodedhash;
        try {
            Resources resources = getResources();
            final String urla = String.format(resources.getString(R.string.web_page_a));

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient client = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(urla);
            httpget.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpget.setHeader("Authorization", basicAuth);
            HttpResponse response = client.execute(httpget);
            int statusCode = response.getStatusLine().getStatusCode();
            Header[] headers = response.getAllHeaders();
            HttpEntity resEntityGet = response.getEntity();
            if (resEntityGet != null) {
                if (statusCode != HttpStatus.SC_OK) {
                    //dlg.dismiss();

                    Toast toast = Toast.makeText(this, "  Invalid Credentials  \n  Access Denied!   ", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    centerText(toast.getView());
                    toast.show();

                   // Log.i("TAG", "Error in web request: " + statusCode + " - " + response.getFirstHeader("InkPon_MSG").getValue());

                } else {
                    //do something with the response
                    String InkPon_MSG = response.getFirstHeader("InkPon_MSG").getValue();
                    String Inkpon_TokenKey = response.getFirstHeader("Inkpon_TokenKey").getValue();
                    if (InkPon_MSG != null && Inkpon_TokenKey != null) {
                        //Log.i("InkPonToken", Inkpon_TokenKey);
                        //Log.i("InkPon_MSG", InkPon_MSG);

                        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
                        editor.putString("Token", Inkpon_TokenKey);
                        editor.apply();

                        isLoggedIn = true;

                        /* remmed out 10/16/2014... will call this service upon clicking on edit profile */
                        Intent i = new Intent(this, GetuserinfoService.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        this.startService(i);

                        /* Now start main menu activity. added 10/11/2014*/
                        Intent c = new Intent(Login.this, MainActivity.class);
                        startActivity(c);

                        /* start the logging service*/


                        finish(); /*--added 10/11/2014. Go ahead and close the login screen after success! */
                    }
                }


            }
        }catch(ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }
    public String atob(String str){
        String decodedDataUsingUTF8 = null;

        byte[] encodedhash = Base64.decode(str, Base64.NO_WRAP);
        try {
            decodedDataUsingUTF8 = new String(encodedhash, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return decodedDataUsingUTF8;
    }
    public String btoa(String str){
        //String token = (Username + ":" + hashed);
        String token = str;

        byte[] data = new byte[0];
        try {
            data = token.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String encodedhash = Base64.encodeToString(data, Base64.NO_WRAP);
        return encodedhash;
    }

    public void savepreference() {
        boolean checked = ((CheckBox) findViewById(R.id.rememberMe)).isChecked();
        if (checked){
            appPrefEditor.putBoolean("RememberMe",true);
            appPrefEditor.putString("InkPonUser",btoa(UserName.getText().toString()+":"+Pass.getText().toString()));
        }else{
            appPrefEditor.putBoolean("RememberMe",false);
            appPrefEditor.remove("InkPonUser");
        }
        appPrefEditor.commit();
    }

    public void rememberMeClicked(View view) {
        savepreference();
    }

    /* use this to center Toast*/
    void centerText(View view) {
        if (view instanceof TextView) {
            ((TextView) view).setGravity(Gravity.CENTER);
        } else if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            int n = group.getChildCount();
            for (int i = 0; i < n; i++) {
                centerText(group.getChildAt(i));
            }
        }
    }

}





