package com.inkpon.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapters.Transactionadapter;

/**
 * Created by hounge on 9/26/14.
 */
public class TransactionHistory extends Activity {
    public static final String Stored_Token = "TokenFile";
    ArrayList<String> title_array = new ArrayList<String>();
    ArrayList<String> notice_array = new ArrayList<String>();
    ArrayList<String> notice_array2 = new ArrayList<String>();
    ArrayList<String> notice_array3 = new ArrayList<String>();
    ArrayList<String> notice_array4 = new ArrayList<String>();
    ListView list;
    Transactionadapter adapter;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transferhistory);
        list = (ListView) findViewById(R.id.textViewFirstName);
        new TheTask().execute();


    }

    class TheTask extends AsyncTask<Void, Void, String> {
        public static final String Stored_Token = "TokenFile";
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        String Token = prefs.getString("Token", null);
        String APICALL = prefs.getString("APICall", null);


        @Override
        protected String doInBackground(Void... params) {
            String str = null;
            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                JSONObject Chat = new JSONObject();
                Chat.put("Token", Token);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(APICALL);
                httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                httppost.setEntity(new StringEntity(Chat.toString(),HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);
                str = EntityUtils.toString(response.getEntity());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("response", str);
            return str;

        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            String response = result.toString();
            try {
                JSONArray new_array = new JSONArray(response);
                for (int i = 0, count = new_array.length(); i < count; i++) {
                    try {
                        JSONObject jsonObject = new_array.getJSONObject(i);
                        title_array.add(jsonObject.getString("Hours").toString());
                        notice_array.add(jsonObject.getString("DateRequested").toString());
                        notice_array2.add(jsonObject.getString("Status").toString());
                        notice_array3.add(jsonObject.getString("Type").toString());
                        notice_array4.add(jsonObject.getString("From").toString());
                        notice_array4.add(jsonObject.getString("To").toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                adapter = new Transactionadapter(TransactionHistory.this, title_array, notice_array,notice_array2,notice_array3,notice_array4);
                list.setAdapter(adapter);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }

        }
    }

    public void Backbutton(View view){
        this.finish();
    }
    public void onReFresh(View view){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    public void timesend(View view){
        Intent i = new Intent(this, SendTime.class);
        startActivity(i);
    }
    public void timerequest(View view){
        Intent i = new Intent(this, RequestTime.class);
        startActivity(i);
    }
    public void transactionbutton(View view){
        Intent i = new Intent(this, TransactionHistory.class);
        startActivity(i);
    }
}
