package com.inkpon.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapters.ChatAdapter;

/**
 * Created by hounge on 9/25/2014.
 */
public class Chat extends Activity {
    public static final String Stored_Token = "TokenFile";
    ArrayList<String> title_array = new ArrayList<String>();

    ListView list;
    ChatAdapter adapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);
        list = (ListView) findViewById(R.id.listView2);
        new TheTask2().execute();
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        String APICALL = prefs.getString("APICall", null);
        Log.i("APICALL", APICALL);
    }
    public void BackButton(View view){
        this.finish();
    }
    public void SendMessage(View view){
        Intent c = new Intent(Chat.this, Compose.class);
        this.startActivity(c);
    }

    class TheTask2 extends AsyncTask<Void, Void, String> {
        public static final String Stored_Token = "TokenFile";
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        String Token = prefs.getString("Token", null);
        String APICALL = prefs.getString("APICall", null);

    @Override
    protected String doInBackground(Void... params) {
        String str = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            JSONObject Deals = new JSONObject();
            Deals.put("Token", Token);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(APICALL);
            httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httppost.setEntity(new StringEntity(Deals.toString(),HTTP.UTF_8));
            HttpResponse response = httpclient.execute(httppost);
            str = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("response", str);
        return str;

    }

    @Override
    protected void onPostExecute(String result) {

        super.onPostExecute(result);
        String response = result.toString();
        try {
            JSONArray new_array = new JSONArray(response);
            for (int i = 0, count = new_array.length(); i < count; i++) {
                try {
                    JSONObject jsonObject = new_array.getJSONObject(i);
                    title_array.add(jsonObject.getString("FullName").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            adapter = new ChatAdapter(Chat.this, title_array);
            list.setAdapter(adapter);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            // tv.setText("error2");
        }

    }
    }
}
