package com.inkpon.app;

/**
 * Created by hounge on 10/4/14.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONException;

import java.io.IOException;

import services.GetuserinfoService;

public class SplashActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1100;
    public static final String Stored_Token = "TokenFile";
    SharedPreferences appPref;
    SharedPreferences.Editor appPrefEditor;

    private ProgressDialog simpleWaitDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();

        /* Get Token and check to see if it is still valid */
        final String Token = appPref.getString("Token", "***");
        Log.d("***Checking if Token is still valid: ", Token);

        setContentView(R.layout.splash);
        //new CheckToken().execute(Token);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent i = new Intent(SplashActivity.this, Login.class);
                //startActivity(i);
                // close this activity
                //finish();
                new CheckToken().execute(Token);
            }
        }, SPLASH_TIME_OUT);


    }


    private class CheckToken extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            return GoValidateToken(params[0].toString());
        }

        @Override
        protected void onPreExecute() {
            simpleWaitDialog = ProgressDialog.show(SplashActivity.this, "Wait", "Validating Token...");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);
            simpleWaitDialog.dismiss();

            if (o.equalsIgnoreCase("main")) {
                /* Valid Token... go straight to Main Menu */
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            } else { //--basically anything else that is NOT main...*/
                /* Invalid token or empty, go to login screen */
                appPrefEditor.remove("Token");
                appPrefEditor.apply();

                Intent i = new Intent(SplashActivity.this, Login.class);
                startActivity(i);
                finish();
            }
        }

        private String GoValidateToken(String Token) {
            String ret = null;
            try {
                String url = getResources().getString(R.string.checktoken);
                JSONObject Details = new JSONObject();
                Details.put("Token", Token);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                httppost.setEntity(new StringEntity(Details.toString(), HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);

                /* Get the response message from CheckToken*/
                String msg = EntityUtils.toString(response.getEntity());
                msg = msg.replaceAll("\"", "");
                Log.d("*** CheckToken is ", msg);

                /*--check http status code...*/
                int statusCode = response.getStatusLine().getStatusCode();

                //System.out.println("**********["+msg+"]***********");

                if (statusCode == HttpStatus.SC_OK) {
                    if (msg.equalsIgnoreCase("Valid")) {
                        //Intent d = new Intent(SplashActivity.this, Main.class);
                        //startActivity(d);
                        //finish(); /*--added 10/11/2014. Go ahead and close the login screen after success! */

                        /*--added this here to get user profile... probability of user checking
                        user profile for update infor is very high. Also keep in mind since it is a valid token, we skip login class where this service also gets called */
                        Intent i = new Intent(SplashActivity.this, GetuserinfoService.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        SplashActivity.this.startService(i);

                        ret = "main";
                    } else {
                        ret = "login";
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return ret;
        }

    }
}