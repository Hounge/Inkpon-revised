package adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.inkpon.app.R;

import java.util.ArrayList;

/**
 * Created by hounge on 10/9/14.
 */
public class Transactionadapter extends BaseAdapter {

    private Activity activity;

    private static ArrayList title, notice,notice2, notice3,notice4 ;
    private static LayoutInflater inflater = null;

    public Transactionadapter(Activity a, ArrayList b, ArrayList bod, ArrayList bod2, ArrayList bod3, ArrayList bod4) {
        activity = a;
        this.title = b;
        this.notice = bod;
        this.notice2 = bod2;
        this.notice3 = bod3;
        this.notice4 = bod4;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return title.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.tranactionlist, null);

        TextView title2 = (TextView) vi.findViewById(R.id.Hours); // title
        String song = title.get(position).toString();
        title2.setText(song);

        TextView title22 = (TextView) vi.findViewById(R.id.Date); // notice
        String song2 = notice.get(position).toString();
        title22.setText(song2);

        TextView title23 = (TextView) vi.findViewById(R.id.Status); // notice
        String song3 = notice2.get(position).toString();
        title23.setText(song3);

        TextView title24 = (TextView) vi.findViewById(R.id.Type); // notice
        String song4 = notice3.get(position).toString();
        title24.setText(song4);

        TextView title25 = (TextView) vi.findViewById(R.id.ToFrom); // notice
        String song5 = notice4.get(position).toString();
        title25.setText(song5);



        return vi;

    }
}