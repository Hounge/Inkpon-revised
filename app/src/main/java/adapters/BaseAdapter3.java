package adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.inkpon.app.R;

import java.util.ArrayList;

/**
 * Created by hounge on 10/5/14.
 */
public class BaseAdapter3 extends BaseAdapter {

    private Activity activity;

    private static ArrayList title, notice;
    private static LayoutInflater inflater = null;

    public BaseAdapter3(Activity a, ArrayList b, ArrayList bod) {
        activity = a;
        this.title = b;
        this.notice = bod;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return title.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.detailslist, null);

        TextView title2 = (TextView) vi.findViewById(R.id.Hours); // title
        String song = title.get(position).toString();
        title2.setText(song);


        TextView title22 = (TextView) vi.findViewById(R.id.Date); // notice
        String song2 = notice.get(position).toString();
        title22.setText(song2);

        ///---alternate row color
        if (position % 2 == 1) {
            vi.setBackgroundColor(Color.parseColor("#ffd9d9d9"));//Color.BLUE);
        } else {
            vi.setBackgroundColor(Color.TRANSPARENT);
        }


        return vi;
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View view = super.getView(position, convertView, parent);
//        if (position % 2 == 1) {
//            view.setBackgroundColor(Color.BLUE);
//        } else {
//            view.setBackgroundColor(Color.CYAN);
//        }
//
//        return view;
//    }

}
